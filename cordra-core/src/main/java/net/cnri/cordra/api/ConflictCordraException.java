package net.cnri.cordra.api;

import com.google.gson.JsonElement;

public class ConflictCordraException extends BadRequestCordraException {

    public ConflictCordraException(String message) {
        super(CordraException.responseForMessage(message), 409);
    }

    public ConflictCordraException(String message, Throwable cause) {
        super(CordraException.responseForMessage(message), 409, cause);
    }

    public ConflictCordraException(Throwable cause) {
        super(409, cause);
    }

    public ConflictCordraException(JsonElement response) {
        super(response, 409);
    }

    public ConflictCordraException(JsonElement response, Throwable cause) {
        super(response, 409, cause);
    }
}
