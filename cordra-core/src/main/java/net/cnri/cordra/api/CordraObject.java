package net.cnri.cordra.api;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.cnri.cordra.util.GsonUtility;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CordraObject {

    // Changes to CordraObject need to be reflected in AttributesUtil to continue to work with legacy storage

    public String id;
    public String type;
    public JsonElement content;
    public AccessControlList acl;
    public JsonObject userMetadata;
    public Metadata metadata;
    public JsonObject responseContext;
    public List<Payload> payloads;

    private transient List<String> payloadsToDelete = new ArrayList<>();

    public CordraObject() { }

    public CordraObject(String type, String json) {
        this.type = type;
        setContent(json);
    }

    public CordraObject(String type, JsonElement content) {
        this.type = type;
        this.content = content;
    }

    public CordraObject(String type, Object object) {
        this.type = type;
        Gson gson = GsonUtility.getGson();
        this.content = gson.toJsonTree(object);
    }

    public void setContent(String json) {
        content = JsonParser.parseString(json);
    }

    public void setContent(Object object) {
        Gson gson = GsonUtility.getGson();
        this.content = gson.toJsonTree(object);
    }

    public String getContentAsString() {
        Gson gson = GsonUtility.getGson();
        return gson.toJson(content);
    }

    public <T> T getContent(Class<T> klass) {
        Gson gson = GsonUtility.getGson();
        return gson.fromJson(content, klass);
    }

    public void addPayload(String name, String filename, String mediaType, InputStream in) {
        if (payloads == null) payloads = new ArrayList<>();
        Payload p = new Payload();
        p.name = name;
        p.filename = filename;
        p.mediaType = mediaType;
        p.setInputStream(in);
        payloads.add(p);
    }

    public void deletePayload(String name) {
        payloadsToDelete.add(name);
        removePayloadFromList(name);
    }

    public List<String> getPayloadsToDelete() {
        return payloadsToDelete;
    }

    public void clearPayloadsToDelete() {
        payloadsToDelete = new ArrayList<>();
    }

    private void removePayloadFromList(String name) {
        if (payloads == null) return;
        payloads.removeIf(p -> p.name.equals(name));
        if (payloads.isEmpty()) payloads = null;
    }

    public static class AccessControlList {
        public List<String> readers;
        public List<String> writers;
        public List<String> payloadReaders; //null means readers can read payloads
        public Map<String, List<String>> methods;

        public static AccessControlList of(AccessControlList acl) {
            AccessControlList result = new AccessControlList();
            if (acl.readers != null) result.readers = new ArrayList<>(acl.readers);
            if (acl.writers != null) result.writers = new ArrayList<>(acl.writers);
            if (acl.payloadReaders != null) result.payloadReaders = new ArrayList<>(acl.payloadReaders);
            if (acl.methods != null) {
                result.methods = new HashMap<>();
                for (Map.Entry<String, List<String>> entry : acl.methods.entrySet()) {
                    result.methods.put(entry.getKey(), new ArrayList<>(entry.getValue()));
                }
            }
            return result;
        }
    }

    public static class Metadata {
        public JsonObject hashes;
        public long createdOn;
        public String createdBy;
        public long modifiedOn;
        public String modifiedBy;
        public Boolean isVersion;
        public String versionOf;
        public String publishedBy;
        public Long publishedOn;
        public String remoteRepository;

        public Long txnId;

        public JsonObject internalMetadata;
    }
}
