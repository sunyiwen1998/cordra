package net.cnri.cordra.api;

public class FacetBucket {
    public String value;
    public long count;
    public String filterQuery;

    public FacetBucket() { }

    public FacetBucket(String value, long count, String filterQuery) {
        this.value = value;
        this.count = count;
        this.filterQuery = filterQuery;
    }
}
