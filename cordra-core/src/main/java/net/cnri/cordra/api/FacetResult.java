package net.cnri.cordra.api;

import java.util.ArrayList;
import java.util.List;

public class FacetResult {
    public String field;
    public List<FacetBucket> buckets = new ArrayList<>();
}
