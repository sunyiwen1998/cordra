package net.cnri.cordra.api;

public class FacetSpecification {

    public String field;

    public FacetSpecification() {}

    public FacetSpecification(String field) {
        this.field = field;
    }
}
