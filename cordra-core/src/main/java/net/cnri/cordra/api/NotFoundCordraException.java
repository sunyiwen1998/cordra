package net.cnri.cordra.api;

import com.google.gson.JsonElement;

public class NotFoundCordraException extends CordraException {

    public NotFoundCordraException(String message) {
        super(CordraException.responseForMessage(message), 404);
    }

    public NotFoundCordraException(String message, Throwable cause) {
        super(CordraException.responseForMessage(message), 404, cause);
    }

    public NotFoundCordraException(Throwable cause) {
        super(404, cause);
    }

    public NotFoundCordraException(JsonElement response) {
        super(response, 404);
    }

    public NotFoundCordraException(JsonElement response, Throwable cause) {
        super(response, 404, cause);
    }
}
