package net.cnri.cordra.api;

import com.google.gson.JsonElement;

public class UnauthorizedCordraException extends CordraException {

    public static final String PASSWORD_CHANGE_REQUIRED_PROPERTY = "passwordChangeRequired";

    public UnauthorizedCordraException(String message, Throwable cause) {
        super(CordraException.responseForMessage(message), 401, cause);
    }

    public UnauthorizedCordraException(String message) {
        super(CordraException.responseForMessage(message), 401);
    }

    public UnauthorizedCordraException(Throwable cause) {
        super(401, cause);
    }

    public UnauthorizedCordraException(JsonElement response, Throwable cause) {
        super(response, 401, cause);
    }

    public UnauthorizedCordraException(JsonElement response) {
        super(response, 401);
    }

    public boolean isPasswordChangeRequired() {
        if (this.getResponse() == null) return false;
        if (!this.getResponse().isJsonObject()) return false;
        if (!this.getResponse().getAsJsonObject().has(PASSWORD_CHANGE_REQUIRED_PROPERTY)) return false;
        JsonElement passwordChangeRequiredElement = this.getResponse().getAsJsonObject().get(PASSWORD_CHANGE_REQUIRED_PROPERTY);
        if (!passwordChangeRequiredElement.isJsonPrimitive()) return false;
        if (!passwordChangeRequiredElement.getAsJsonPrimitive().isBoolean()) return false;
        return passwordChangeRequiredElement.getAsJsonPrimitive().getAsBoolean();
    }
}
