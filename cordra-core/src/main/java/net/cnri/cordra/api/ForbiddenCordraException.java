package net.cnri.cordra.api;

import com.google.gson.JsonElement;

public class ForbiddenCordraException extends CordraException {

    public ForbiddenCordraException(String message) {
        super(CordraException.responseForMessage(message), 403);
    }

    public ForbiddenCordraException(String message, Throwable cause) {
        super(CordraException.responseForMessage(message), 403, cause);
    }

    public ForbiddenCordraException(Throwable cause) {
        super(403, cause);
    }

    public ForbiddenCordraException(JsonElement response) {
        super(response, 403);
    }

    public ForbiddenCordraException(JsonElement response, Throwable cause) {
        super(response, 403, cause);
    }
}
