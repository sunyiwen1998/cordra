(function () {
    "use strict";

    function elementForSuggestion(searchResult) {
        var handleString = searchResult.id;
        var searchResultDiv = $('<div class="search-result col-md-12"/>');
        searchResultDiv.attr("data-handle", handleString);
        var previewData = getPreviewData(searchResult);
        var headerRow = $('<div class="header row"/>');
        searchResultDiv.append(headerRow);
        var header = $('<h4 class="col-md-12"></h4>');
        headerRow.append(header);
        var link = $('<a class="list-handles-link" target="_blank">')
            .attr("href", "#objects/" + handleString)
            .text(handleString);
        link.attr("data-handle", handleString);

        for (var jsonPointer in previewData) {
            var thisPreviewData = previewData[jsonPointer];
            var prettifiedPreviewData = prettifyPreviewJson(
                thisPreviewData.previewJson
            );
            if (!prettifiedPreviewData) continue;

            if (thisPreviewData.isPrimary) {
                link = $('<a class="list-handles-link" target="_blank">')
                    .attr("href", "#objects/" + handleString)
                    .text(prettifiedPreviewData);
                link.attr("data-handle", handleString);
            }
        }
        header.prepend(link);
        var objectIdSpan = $(
            '<div class="header-id col-md-12" title="Object ID" data-original-title="ObjectID"></div>'
        );
        var objectIdLink = $('<a class="list-handles-link" target="_blank">')
            .attr("href", "#objects/" + handleString)
            .text(handleString);
        objectIdLink.attr("data-handle", handleString);
        objectIdSpan.append(objectIdLink);

        headerRow.append(objectIdSpan);
        return searchResultDiv;
    }

    function prettifyPreviewJson(previewJson, maxLength) {
        var result = null;
        if (typeof previewJson === "string") {
            result = previewJson;
        } else {
            result = JSON.stringify(previewJson);
        }
        if (maxLength != null && maxLength != undefined) {
            if (result.length > maxLength) {
                result = result.substring(0, maxLength) + "...";
            }
        }
        return result;
    }

    function getPreviewData(searchResult) {
        var res = {};
        var schema = APP.getSchema(searchResult.type);
        if (!schema) return res;
        var content = searchResult.content;
        if (!content) content = searchResult.json; // old-style search result
        var pointerToSchemaMap = SchemaExtractorFactory.get().extract(
            content,
            schema
        );
        var foundPrimary = false;
        for (var jsonPointer in pointerToSchemaMap) {
            var subSchema = pointerToSchemaMap[jsonPointer];
            var previewNode = SchemaUtil.getDeepCordraSchemaProperty(
                subSchema,
                "preview"
            );
            if (!previewNode) continue;
            var showInPreview = previewNode["showInPreview"];
            var isPrimary = previewNode["isPrimary"];
            var excludeTitle = previewNode["excludeTitle"];
            if (!showInPreview) continue;
            var title = subSchema["title"];
            if (!title) title = jsonPointer;
            var previewJson = JsonUtil.getJsonAtPointer(content, jsonPointer);
            var data = { title: title, previewJson: previewJson };
            if (subSchema.format === "uri") {
                data.isUri = true;
            }
            if (isPrimary && !foundPrimary) {
                data.isPrimary = true;
                foundPrimary = true;
            }
            if (excludeTitle) {
                data.excludeTitle = true;
            }
            res[jsonPointer] = data;
        }
        return res;
    }

    var ObjectPreviewUtil = {};
    ObjectPreviewUtil.elementForSuggestion = elementForSuggestion;
    ObjectPreviewUtil.getPreviewData = getPreviewData;
    ObjectPreviewUtil.prettifyPreviewJson = prettifyPreviewJson;
    window.ObjectPreviewUtil = ObjectPreviewUtil;
})();
