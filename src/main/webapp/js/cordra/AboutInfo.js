function AboutInfo(containerDiv, version) {
    var self = this;

    function constructor() {
        var html = $(`
        <div id='version' style='text-align: center;'>
            <img width="80" src="img/cordra-primary-blue-large.png" alt="logo">
            <h3>Cordra</h3>
            <p id='versionNumber'></p>
            <p id='versionTimestamp'></p>
            <p id='versionBuild'></p>
        </div>
        `);
        containerDiv.append(html);

        var number = containerDiv.find('#versionNumber');
        var timestamp = containerDiv.find('#versionTimestamp');
        var build = containerDiv.find('#versionBuild');
        number.text("Version: " + version.number);
        timestamp.text("Date: " + version.timestamp);
        build.text("Build id: " + version.id);
    }

    constructor();
}