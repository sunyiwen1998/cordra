function ObjectMethods(containerDiv, objectId, type, contentPlusMeta) {
    var self = this;

    var methodSelect = null;
    var methodParamsDiv = null;
    var methodParamsEditor = null;
    var outputDiv = null;
    var outputEditor = null;

    var methodsList = [];
    var typeForCall;

    function constructor() {
        if (type === "Schema") {
            typeForCall = contentPlusMeta.content.name;
        } else {
            typeForCall = type;
        }

        var html = $(`
            <div class="object-editor-toolbar col-md-12 pull-right">
                <form id="methodsForm" class="form-horizontal" role="form">
                    <div class="form-group">
                        <label for="methodSelect" class="col-sm-1 control-label">Method</label>
                        <div class="col-sm-8">
                            <select id="methodSelect" class="form-control"></select>
                        </div>
                        <button id="callButton" class="col-sm-2 btn btn-sm btn-primary"><i class="fa fa-clone"></i>
                            <span>Call Method</span>
                        </button>
                    </div>
                </form>
            </div>
                <div class="card card-body bg-light" id=ioDiv>
                <div class="form-group col-md-6" id=params>
                    <label for="methodParams" style="display: block;">Params</label>
                    <div id="methodParams" style="height: 500px"></div>
                </div>
                <div class="form-group col-md-6" id=output>
                    <label for="methodOutput" style="display: block;">Output</label>
                    <div id="methodOutput" style="height: 500px"></div>
                </div>
            </div>            
        `);
        containerDiv.append(html);

        var callButton = $('#callButton');
        callButton.on("click", onCallMethodClick);

        var methodsForm = $('#methodsForm');
        methodsForm.on("submit", function (e) {
            return false;
        });

        methodSelect = $('#methodSelect');
        methodParamsDiv = $("#methodParams"); 
        outputDiv = $("#methodOutput"); 

        var options = {
            ace: ace,
            theme: "ace/theme/textmate",
            mode: "code",
            modes: ["code", "tree"], // allowed modes
            onError: function (err) {
                alert(err.toString());
            }
        };
        methodParamsEditor = new JsonEditorOnline(methodParamsDiv[0], options, {});

        var options = {
            ace: ace,
            theme: "ace/theme/textmate",
            mode: "code",
            modes: ["code", "tree"], // allowed modes
            onError: function (err) {
                alert(err.toString());
            }
        };
        outputEditor = new JsonEditorOnline(outputDiv[0], options);
        outputEditor.setText("");
        APP.disableJsonEditorOnline(outputEditor);
        listMethods();
    }

    function onCallMethodClick() {
        callMethod();
    }

    function listMethods() {
        let typeParam = type;
        if (type === "Schema") {
            typeParam = contentPlusMeta.content.name;
        } 
        APP.listInstanceAndStaticMethods(objectId, typeParam, onListMethodsSuccess, onListMethodsError);
    }

    function onListMethodsSuccess(methods) {
        console.log(methods);
        methodsList = [];
        methods.instanceMethods.sort();
        methods.staticMethods.sort();
        for (const methodName of methods.instanceMethods) {
            let method = {
                isStatic: false,
                name: methodName
            };
            methodsList.push(method);
        }
        for (const methodName of methods.staticMethods) {
            let method = {
                isStatic: true,
                name: methodName
            };
            methodsList.push(method);
        }
        for (const method of methodsList) {
            let displyName = method.name;
            if (method.isStatic) {
                displyName += " [static]";
            }
            var option = $(
                '<option value="' + method.name + '">' + displyName + "</option>"
            );
            methodSelect.append(option);
        }
    }

    function onListMethodsError(response) { }

    function callMethod() {
        var selectedIndex = methodSelect.prop('selectedIndex');
        var method = methodsList[selectedIndex];
        var paramsText = methodParamsEditor.getText();
        var params = undefined;
        if (paramsText) {
            try {
                params = JSON.parse(paramsText);
            } catch (error) {
                APP.notifications.alertError("Params are not valid JSON.");
                return;
            }
        }
        if (method.isStatic) {
            APP.callMethodForTypeAsResponse(
                typeForCall,
                method.name,
                params,
                onCallSuccess,
                onCallError
            );
        } else {
            APP.callMethodAsResponse(
                objectId,
                method.name,
                params,
                onCallSuccess,
                onCallError
            );
        }
    }

    function onCallSuccess(response) { 
        response.text()
            .then(function(responseText) {
                try {
                    let json = JSON.parse(responseText);
                    var prettyText = JSON.stringify(json, null, 2);
                    outputEditor.setText(prettyText);
                } catch (error) {
                    outputEditor.setText(responseText);
                    console.log(response);
                }

            });
    }

    function onCallError() { }

    constructor();
}

