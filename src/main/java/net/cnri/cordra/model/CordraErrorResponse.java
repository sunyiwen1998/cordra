package net.cnri.cordra.model;

public class CordraErrorResponse {
    @SuppressWarnings("unused") // for serialization
    private final String message;

    public CordraErrorResponse(String message) {
        this.message = message;
    }
}
