package net.cnri.cordra;

import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonWriter;
import net.cnri.cordra.api.CordraObject;
import net.cnri.cordra.api.FacetResult;
import net.cnri.cordra.api.QueryParams;
import net.cnri.cordra.api.SearchResults;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.Writer;
import java.util.List;

public class CordraSearcher {
    private static final Logger logger = LoggerFactory.getLogger(CordraSearcher.class);

    public static void writeResults(SearchResults<String> results, Writer writer, QueryParams params) throws IOException {
        @SuppressWarnings("resource")
        JsonWriter jsonWriter = new JsonWriter(writer);
        jsonWriter.setIndent("  ");
        jsonWriter.beginObject();
        jsonWriter.name("pageNum").value(params.getPageNum());
        jsonWriter.name("pageSize").value(params.getPageSize());
        jsonWriter.name("size").value(results.size());
        List<FacetResult> facetResults = null;
        try {
            facetResults = results.getFacets();
        } catch (IncompatibleClassChangeError e) {
            logger.warn("Unexpected mismatch of cordra jar versions on call to SearchResults.getFacets");
        }
        if (facetResults != null) {
            jsonWriter.name("facets");
            GsonUtility.getGson().toJson(facetResults, new TypeToken<List<FacetResult>>() { }.getType(), jsonWriter);
        }
        jsonWriter.name("results").beginArray();
        for (String id : results) {
            GsonUtility.getGson().toJson(id, String.class, jsonWriter);
        }
        jsonWriter.endArray();
        jsonWriter.endObject();
        jsonWriter.flush();
    }

    public static void writeResults(SearchResults<CordraObject> results, Writer writer, QueryParams params, boolean isFull) throws IOException {
        @SuppressWarnings("resource")
        JsonWriter jsonWriter = new JsonWriter(writer);
        jsonWriter.setIndent("  ");
        jsonWriter.beginObject();
        jsonWriter.name("pageNum").value(params.getPageNum());
        jsonWriter.name("pageSize").value(params.getPageSize());
        jsonWriter.name("size").value(results.size());
        List<FacetResult> facetResults = null;
        try {
            facetResults = results.getFacets();
        } catch (IncompatibleClassChangeError e) {
            logger.warn("Unexpected mismatch of cordra jar versions on call to SearchResults.getFacets");
        }
        if (facetResults != null) {
            jsonWriter.name("facets");
            GsonUtility.getGson().toJson(facetResults, new TypeToken<List<FacetResult>>() { }.getType(), jsonWriter);
        }
        jsonWriter.name("results").beginArray();
        for (CordraObject co : results) {
            if (isFull) {
                GsonUtility.getGson().toJson(co, CordraObject.class, jsonWriter);
            } else {
                GsonUtility.getGson().toJson(co.content, jsonWriter);
            }
        }
        jsonWriter.endArray();
        jsonWriter.endObject();
        jsonWriter.flush();
    }
}
