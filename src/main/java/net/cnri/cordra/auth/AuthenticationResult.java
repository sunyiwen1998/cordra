package net.cnri.cordra.auth;

import java.util.List;

public class AuthenticationResult {
    public boolean active; //success
    public boolean anonymous;
    public String userId;
    public String username;
    public List<String> groupIds;
    public Boolean grantAuthenticatedAccess; // only potentially null in the raw result from the authenticate hook
    public Long exp;

    public List<String> hookSpecifiedGroupIds;
    public boolean bypassCordraGroupObjects;

    public AuthenticationResult() {}

    public AuthenticationResult(boolean active, String userId, String username, List<String> groupIds, boolean grantAuthenticatedAccess, Long exp) {
        this.active = active;
        this.userId = userId;
        this.username = username;
        this.groupIds = groupIds;
        this.grantAuthenticatedAccess = grantAuthenticatedAccess;
        this.exp = exp;
    }

    public static AuthenticationResult anonymous() {
        AuthenticationResult authResult = new AuthenticationResult();
        authResult.anonymous = true;
        authResult.grantAuthenticatedAccess = false; // to ensure never null when actually used
        return authResult;
    }

    public static AuthenticationResult admin() {
        return new AuthenticationResult(true, "admin", "admin", null, false, null);
    }
}
