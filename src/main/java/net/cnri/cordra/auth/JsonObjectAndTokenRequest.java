package net.cnri.cordra.auth;

import com.google.gson.JsonObject;

public class JsonObjectAndTokenRequest {
    public JsonObject jsonObject;
    public TokenRequest tokenRequest;

    public JsonObjectAndTokenRequest() {
        // no-op
    }

    public JsonObjectAndTokenRequest(JsonObject jsonObject, TokenRequest tokenRequest) {
        this.jsonObject = jsonObject;
        this.tokenRequest = tokenRequest;
    }

}
