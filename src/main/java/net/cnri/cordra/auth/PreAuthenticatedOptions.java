package net.cnri.cordra.auth;

import java.util.List;

public class PreAuthenticatedOptions extends InternalRequestOptions {
    public boolean grantAuthenticatedAccess;
    public List<String> hookSpecifiedGroupIds;
    public boolean bypassCordraGroupObjects;
    public Long exp;
}
