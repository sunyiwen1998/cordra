package net.cnri.cordra.auth;

public abstract class ResultCallbackOptions extends InternalRequestOptions {
    public abstract void callback(AuthenticationResult result);
}
