package net.cnri.cordra.auth;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;

import net.cnri.cordra.*;
import net.cnri.cordra.api.AccessTokenResponse;
import net.cnri.cordra.api.AuthTokenResponse;
import net.cnri.cordra.api.BadRequestCordraException;
import net.cnri.cordra.api.CordraException;
import net.cnri.cordra.api.ForbiddenCordraException;
import net.cnri.cordra.api.InternalErrorCordraException;
import net.cnri.cordra.api.Options;
import net.cnri.cordra.api.UnauthorizedCordraException;
import net.cnri.cordra.web.ServletErrorUtil;
import net.cnri.cordra.web.ServletUtil;
import net.cnri.servletcontainer.sessions.HttpSessionManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(value = "/auth/token", asyncSupported = true)
public class AuthTokenServlet extends HttpServlet {
    private static Logger logger = LoggerFactory.getLogger(AuthTokenServlet.class);

    private static final String JSON_OBJECT_AND_TOKEN_REQUEST_ATT = AuthTokenServlet.class.getName() + ".JsonObjectAndTokenRequest";

    private static Gson gson = GsonUtility.getGson();

    private HttpSessionManager sessionManager;
    private InternalCordraClient internalCordra;

    @Override
    public void init() throws ServletException {
        super.init();
        try {
            sessionManager = (HttpSessionManager) getServletContext().getAttribute(HttpSessionManager.class.getName());
            internalCordra = InternalCordraClientFactory.get();
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            throwForAuthIfDisallowedAuthenticatingOverHttp(req);
            JsonObjectAndTokenRequest jsonObjectAndTokenRequest = getTokenRequest(req);
            if (jsonObjectAndTokenRequest == null) {
                error(resp, "invalid_request", "Invalid request", null);
                return;
            }
            Options options = buildOptions(req, jsonObjectAndTokenRequest);
            options.full = ServletUtil.getBooleanParameter(req, "full");
            AuthTokenResponse authTokenResponse = internalCordra.getAuthToken(options, atts -> sessionManager.getSession(req, null, true, atts).getId());
            writeSuccessResponse(authTokenResponse, resp);
        } catch (AuthenticationBackOffCordraException e) {
            if (internalCordra.isDisableBackOffRequestParking()) {
                JsonObject errorObject = new JsonObject();
                errorObject.addProperty("error", "too_many_requests");
                errorObject.addProperty("error_description", e.getMessage());
                errorObject.addProperty("message", e.getMessage());
                ServletErrorUtil.writeCordraException(resp, new BadRequestCordraException(errorObject, 429, e));
            } else {
                AuthenticationBackOffFilter.handle(req, resp, e);
            }
        } catch (InternalErrorCordraException e) {
            logger.error("Exception in POST /auth/token", e);
            ServletErrorUtil.internalServerError(resp);
        } catch (UnauthorizedCordraException e) {
            error(resp, "invalid_grant", e.getMessage(), e.isPasswordChangeRequired());
        } catch (CordraException e) {
            ServletErrorUtil.writeCordraException(resp, e);
        } catch (Exception e) {
            logger.error("Exception in POST /auth/token", e);
            ServletErrorUtil.internalServerError(resp);
        }
    }

    private void writeSuccessResponse(AuthTokenResponse authTokenResponse, HttpServletResponse resp) throws IOException {
        AccessTokenResponse successResponse = new AccessTokenResponse();
        successResponse.access_token = authTokenResponse.token;
        successResponse.token_type = "Bearer";
        successResponse.active = true;
        successResponse.userId = authTokenResponse.userId;
        successResponse.username = authTokenResponse.username;
        successResponse.typesPermittedToCreate = authTokenResponse.typesPermittedToCreate;
        successResponse.groupIds = authTokenResponse.groupIds;
        successResponse.exp = authTokenResponse.exp;
        resp.setStatus(HttpServletResponse.SC_OK);
        resp.setHeader("Cache-Control", "no-store");
        resp.setHeader("Pragma", "no-cache");
        respondAsJson(successResponse, resp);
    }

    private InternalRequestOptions buildOptions(HttpServletRequest req, JsonObjectAndTokenRequest jsonObjectAndTokenRequest) {
        // this caching is also used to prevent backing off the same request twice; see AuthenticationBackOffFilter.handle
        InternalRequestOptions options = (InternalRequestOptions) req.getAttribute(ServletAuthUtil.INTERNAL_REQUEST_OPTIONS_ATT_NAME);
        if (options != null) return options;
        options = new InternalRequestOptions();
        options.authTokenInput = jsonObjectAndTokenRequest.jsonObject;
        req.setAttribute(ServletAuthUtil.INTERNAL_REQUEST_OPTIONS_ATT_NAME, options);
        return options;
    }

    public static JsonObjectAndTokenRequest getTokenRequest(HttpServletRequest req) throws CordraException, IOException {
        Object tokenRequestAttribute = req.getAttribute(JSON_OBJECT_AND_TOKEN_REQUEST_ATT);
        if (tokenRequestAttribute != null) {
            return (JsonObjectAndTokenRequest) tokenRequestAttribute;
        }
        String mediaType = req.getContentType();
        if (mediaType == null) {
            return null;
        }
        if (mediaType.contains(";")) {
            mediaType = mediaType.substring(0, mediaType.indexOf(";")).trim();
        }
        if ("application/x-www-form-urlencoded".equalsIgnoreCase(mediaType)) {
            TokenRequest tokenRequest = new TokenRequest();
            tokenRequest.grant_type = req.getParameter("grant_type");
            tokenRequest.username = req.getParameter("username");
            tokenRequest.password = req.getParameter("password");
            tokenRequest.assertion = req.getParameter("assertion");
            tokenRequest.token = req.getParameter("token");
            JsonObject jsonObject = GsonUtility.getGson().toJsonTree(tokenRequest).getAsJsonObject();
            JsonObjectAndTokenRequest jsonObjectAndTokenRequest = new JsonObjectAndTokenRequest(jsonObject, tokenRequest);
            req.setAttribute(JSON_OBJECT_AND_TOKEN_REQUEST_ATT, jsonObjectAndTokenRequest);
            return jsonObjectAndTokenRequest;
        } else if ("application/json".equalsIgnoreCase(mediaType)) {
            try {
                JsonElement jsonElement = JsonParser.parseReader(req.getReader());
                if (!jsonElement.isJsonObject()) throw error("invalid_request", "Invalid request", null);
                JsonObject jsonObject = jsonElement.getAsJsonObject();
                TokenRequest tokenRequest = gson.fromJson(jsonObject, TokenRequest.class);
                JsonObjectAndTokenRequest jsonObjectAndTokenRequest = new JsonObjectAndTokenRequest(jsonObject, tokenRequest);
                req.setAttribute(JSON_OBJECT_AND_TOKEN_REQUEST_ATT, jsonObjectAndTokenRequest);
                return jsonObjectAndTokenRequest;
            } catch (JsonParseException e) {
                throw error("invalid_request", "Invalid request", null);
            }
        } else {
            return null;
        }
    }

    public static void handleCordraDefaultAuthTokenErrors(Options options) throws BadRequestCordraException {
        TokenRequest tokenRequest;
        try {
            tokenRequest = gson.fromJson(options.authTokenInput, TokenRequest.class);
        } catch (JsonParseException e) {
            throw error("invalid_request", "Invalid request", null);
        }
        if ("urn:ietf:params:oauth:grant-type:jwt-bearer".equals(tokenRequest.grant_type)) {
            if (tokenRequest.assertion == null) {
                throw error("invalid_request", "Invalid request", null);
            }
        } else if ("password".equals(tokenRequest.grant_type)) {
            if (tokenRequest.username == null || tokenRequest.password == null) {
                throw error("invalid_request", "Invalid request", null);
            }
        } else if (tokenRequest.grant_type == null) {
            if (tokenRequest.username == null || tokenRequest.password == null) {
                throw error("invalid_request", "Invalid request", null);
            }
        } else {
            throw error("unsupported_grant_type", "Unsupported grant_type", null);
        }
    }

    protected void respondAsJson(Object o, HttpServletResponse resp) throws IOException {
        try {
            gson.toJson(o, resp.getWriter());
        } catch (JsonIOException e) {
            throw new IOException("Unable to write JSON", e);
        }
        resp.getWriter().println();
    }

    private void error(HttpServletResponse resp, String error, String message, Boolean isPasswordChangeRequired) throws IOException {
        resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        ErrorResponse response = new ErrorResponse();
        response.error = error;
        response.error_description = message;
        response.message = message;
        response.passwordChangeRequired = isPasswordChangeRequired;
        respondAsJson(response, resp);
    }

    public static BadRequestCordraException error(String error, String message, Boolean isPasswordChangeRequired) {
        ErrorResponse response = new ErrorResponse();
        response.error = error;
        response.error_description = message;
        response.message = message;
        response.passwordChangeRequired = isPasswordChangeRequired;
        return new BadRequestCordraException(GsonUtility.getGson().toJsonTree(response));
    }

    public static void throwForAuthIfDisallowedAuthenticatingOverHttp(HttpServletRequest req) throws CordraException {
        try {
            ServletAuthUtil.throwIfDisallowedAuthenticatingOverHttp(req);
        } catch (ForbiddenCordraException e) {
            throw error("invalid_request", "Authentication requires HTTPS", null);
        }
    }

    @SuppressWarnings("unused")
    public static class ErrorResponse {
        public String error;
        public String error_description;
        public String message;
        public Boolean passwordChangeRequired;
    }
}
