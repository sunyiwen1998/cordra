package net.cnri.cordra.auth;

import java.util.Map;

public interface AuthConfigInterface {

    Map<String, DefaultAcls> getSchemaAcls();
    DefaultAcls getDefaultAcls();
    DefaultAcls getAclForObjectType(String objectType);
}
