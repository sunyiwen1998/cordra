package net.cnri.cordra.indexer.elasticsearch;

import net.cnri.cordra.api.FacetBucket;
import net.cnri.cordra.api.FacetResult;
import net.cnri.cordra.api.InternalErrorCordraException;
import net.cnri.cordra.api.UncheckedCordraException;
import net.cnri.cordra.collections.AbstractSearchResults;
import net.cnri.cordra.util.SearchUtil;

import org.elasticsearch.action.search.ClearScrollRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchScrollRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ElasticScrollableSearchResults extends AbstractSearchResults<SearchHit> {

    private SearchResponse results;
    private Iterator<SearchHit> hitsIter;
    private final RestHighLevelClient client;
    private final RequestOptions requestOptions;

    public ElasticScrollableSearchResults(SearchResponse results, RestHighLevelClient client, RequestOptions requestOptions) {
        this.results = results;
        this.client = client;
        this.requestOptions = requestOptions;
    }

    @Override
    public int size() {
        return (int) results.getHits().getTotalHits();
    }

    @Override
    public List<FacetResult> getFacets() {
        Aggregations aggregations = results.getAggregations();
        if (aggregations == null) return null;
        List<FacetResult> facets = new ArrayList<>();
        for (Aggregation agg : aggregations.asList()) {
            String type = agg.getType();
            if ("sterms".equals(type)) {
                FacetResult facet = new FacetResult();
                facet.field = agg.getName();
                facets.add(facet);
                Terms termsAgg = (Terms)agg;
                List<? extends Terms.Bucket> buckets = termsAgg.getBuckets();
                for (Terms.Bucket b : buckets) {
                    String value = b.getKeyAsString();
                    // TODO consider backslash-escaping spaces instead of putting in double-quotes
                    String filterQuery = "sort_"+ SearchUtil.escape(facet.field) + ":\"" + SearchUtil.escape(value) + "\"";
                    FacetBucket bucket = new FacetBucket(value, b.getDocCount(), filterQuery);
                    facet.buckets.add(bucket);
                }
            }
        }
        return facets;
    }

    @Override
    protected SearchHit computeNext() {
        if (hitsIter == null) {
            hitsIter = results.getHits().iterator();
        }
        if (hitsIter.hasNext()) {
            return hitsIter.next();
        } else {
            try {
                results = getNextScrollResults(results.getScrollId());
                if (results == null) {
                    return null;
                } else {
                    hitsIter = results.getHits().iterator();
                    if (hitsIter.hasNext()) {
                        return hitsIter.next();
                    } else {
                        return null;
                    }
                }
            } catch (Exception e) {
                throw new UncheckedCordraException(new InternalErrorCordraException(e));
            }
        }
    }

    private SearchResponse getNextScrollResults(String scrollId) throws IOException {
        if (scrollId == null) {
            return null;
        }
        SearchScrollRequest scrollRequest = new SearchScrollRequest(scrollId);
        scrollRequest.scroll("60s");
        return client.scroll(scrollRequest, requestOptions);
    }

    @Override
    protected void closeOnlyOnce() {
        if (results != null) {
            String scrollId = results.getScrollId();
            if (scrollId != null) {
                ClearScrollRequest clearScrollRequest = new ClearScrollRequest();
                clearScrollRequest.addScrollId(scrollId);
                try {
                    client.clearScroll(clearScrollRequest, requestOptions);
                } catch (Exception e) {
                    // ignore
                }
            }
        }
    }
}
