package net.cnri.cordra;

import java.util.Map;

import com.fasterxml.jackson.databind.JsonNode;

public class DesignPlusSchemas extends Design {

    public Map<String, String> schemaIds;
    public Map<String, JsonNode> schemas; // map from name to schema json
    public Map<String, String> baseUriToNameMap; // map from a schemas baseUri to its name
    public Map<String, String> nameToBaseUriMap; // map from a schema name to its baseUri

    public DesignPlusSchemas(Map<String, JsonNode> schemas, Design design, Map<String, String> schemaIds) {
        super(design);
        this.schemaIds = schemaIds;
        this.schemas = schemas;
    }
}
