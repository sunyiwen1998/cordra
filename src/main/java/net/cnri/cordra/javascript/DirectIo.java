package net.cnri.cordra.javascript;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;

import net.cnri.util.StreamUtil;

public interface DirectIo {
    InputStream getInputAsInputStream() throws IOException;
    Reader getInputAsReader() throws IOException;

    default byte[] getInputAsBytes() throws IOException {
        try (InputStream in = getInputAsInputStream()) {
            if (in == null) return null;
            return StreamUtil.readFully(in);
        }
    }

    default String getInputAsString() throws IOException {
        try (Reader reader = getInputAsReader()) {
            if (reader == null) return null;
            return StreamUtil.readFully(reader);
        }
    }

    OutputStream getOutputAsOutputStream() throws IOException;
    Writer getOutputAsWriter() throws IOException;

    default void writeOutputBytes(byte[] output) throws IOException {
        getOutputAsOutputStream().write(output);
    }

    default void writeOutputString(String output) throws IOException {
        getOutputAsWriter().write(output);
    }

    String getInputMediaType();
    String getInputFilename();
    void setOutputMediaType(String mediaType);
    void setOutputFilename(String filename);
}
