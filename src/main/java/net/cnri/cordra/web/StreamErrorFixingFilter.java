package net.cnri.cordra.web;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.ServletResponseWrapper;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

public class StreamErrorFixingFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // no-op
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        if (response instanceof StreamErrorFixingHttpServletResponseWrapper) {
            chain.doFilter(request, response);
            return;
        }
        if (response instanceof HttpServletResponseWrapper) {
            if (((ServletResponseWrapper)response).isWrapperFor(StreamErrorFixingHttpServletResponseWrapper.class)) {
                chain.doFilter(request, response);
                return;
            }
        }
        chain.doFilter(request, new StreamErrorFixingHttpServletResponseWrapper((HttpServletResponse)response));
    }

    @Override
    public void destroy() {
        // no-op
    }

}
