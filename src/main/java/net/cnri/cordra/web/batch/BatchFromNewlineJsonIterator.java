package net.cnri.cordra.web.batch;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import net.cnri.cordra.api.BadRequestCordraException;
import net.cnri.cordra.api.CordraObject;
import net.cnri.cordra.api.InternalErrorCordraException;
import net.cnri.cordra.api.UncheckedCordraException;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class BatchFromNewlineJsonIterator implements Iterator<CordraObject>  {

    private final BufferedReader reader;
    private String nextLine;
    private boolean done;

    public BatchFromNewlineJsonIterator(BufferedReader reader) {
        this.reader = reader;
    }

    @Override
    public boolean hasNext() {
        try {
            if (done) return false;
            if (nextLine != null) return true;
            nextLine = reader.readLine();
            if (nextLine != null) return true;
            done = true;
            return false;
        } catch (IOException e) {
            throw new UncheckedCordraException(new InternalErrorCordraException(e));
        }
    }

    @Override
    public CordraObject next() {
        if (!hasNext()) {
            throw new NoSuchElementException();
        }
        try {
            String lineJson = nextLine;
            nextLine = null;
            JsonObject obj = JsonParser.parseString(lineJson).getAsJsonObject();
            CordraObject co = BatchUpload.fromJsonWithPayloadsAsStrings(obj);
            return co;
        } catch (JsonSyntaxException e) {
            throw new UncheckedCordraException(new BadRequestCordraException("Unable to parse object", e));
        } catch (Exception e) {
            throw new UncheckedCordraException(new InternalErrorCordraException(e));
        }
    }
}
