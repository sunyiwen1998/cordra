package net.cnri.cordra.web.batch;

import com.google.gson.*;
import com.google.gson.stream.JsonWriter;
import net.cnri.cordra.GsonUtility;
import net.cnri.cordra.api.*;

import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BatchUpload {
    private static Logger logger = LoggerFactory.getLogger(BatchUpload.class);

    public enum Format { NEWLINE_SEPARATED_JSON, JSON_ARRAY_OR_SEARCH_RESULTS }

    private static Gson gson = GsonUtility.getGson();

    private final CordraClient cordra;
    private final Options options;
    private final Format format;
    private final AtomicLong count = new AtomicLong(0L);
    private final BufferedReader reader;
    private final JsonWriter jsonWriter;
    private final boolean isFailFast;
    private final boolean isParallel;
    private volatile boolean running = false;
    private boolean isErrors = false;
    private ExecutorService exec;
    public static final int NUMBER_OF_THREADS = 48;
    public static final int TASK_QUEUE_SIZE = NUMBER_OF_THREADS * 2;

    public BatchUpload(BufferedReader reader, Writer writer, CordraClient cordra,
            Options options, Format format, boolean isFailFast, boolean isParallel) {
        this.cordra = cordra;
        this.options = options;
        this.reader = reader;
        this.format = format;
        this.isFailFast = isFailFast;
        this.isParallel = isParallel;
        this.jsonWriter = new JsonWriter(writer);
    }

    public void run() throws IOException, BadRequestCordraException {
        if (running) {
            return;
        } else {
            running = true;
        }
        beginResults();
        if (isParallel) {
            this.exec = new ThreadPoolExecutor(NUMBER_OF_THREADS, NUMBER_OF_THREADS, 0, TimeUnit.MILLISECONDS,
                    new ArrayBlockingQueue<Runnable>(TASK_QUEUE_SIZE), new ThreadPoolExecutor.CallerRunsPolicy());
        }
        Iterator<CordraObject> iter;
        if (format == Format.NEWLINE_SEPARATED_JSON) {
            iter = new BatchFromNewlineJsonIterator(reader);
        } else {
            iter = new BatchFromJsonIterator(reader);
        }
        loadFromIterator(iter);
        endResults();
    }

    private void loadFromIterator(Iterator<CordraObject> iter) {
        while (true) {
            if (!running) {
                break;
            }
            long position = count.getAndIncrement();
            try {
                try {
                    if (!iter.hasNext()) break;
                    CordraObject co = iter.next();
                    if (isParallel) {
                        exec.submit(() -> process(co, position));
                    } else {
                        process(co, position);
                    }
                    if (!running) {
                        break;
                    }
                } catch (UncheckedCordraException ue) {
                    throw ue.getCause();
                }
            } catch (CordraException e) {
                handleResult(Result.from(e, position));
                if (isFailFast) {
                    running = false;
                    break;
                }
            }
        }
        consumeInput();
        if (isParallel) {
            exec.shutdown();
            try { exec.awaitTermination(2, TimeUnit.DAYS); } catch (InterruptedException e) { Thread.currentThread().interrupt(); }
        }
    }

    private void consumeInput() {
        try {
            char[] buf = new char[8192];
            while (reader.read(buf) > 0) { }
        } catch (Exception e) {
            logger.warn("Exception closing input", e);
        }
    }

    private synchronized void handleResult(Result result) {
        GsonUtility.getGson().toJson(result, Result.class, jsonWriter);
        if (result.responseCode != 200) {
            isErrors = true;
        }
    }

    private void beginResults() throws IOException {
        jsonWriter.setIndent("  ");
        jsonWriter.beginObject();
        jsonWriter.name("results").beginArray();
    }

    private void endResults() throws IOException {
        jsonWriter.endArray();
        boolean success = !isErrors;
        jsonWriter.name("success").value(success);
        jsonWriter.endObject();
        jsonWriter.flush();
    }

    private void process(CordraObject co, long position) {
        if (!running) {
            return;
        }
        try {
            CordraObject resultCo = createOrUpdate(co);
            handleResult(Result.from(resultCo, position));
        } catch (CordraException e) {
            if (isFailFast) {
                running = false;
            }
            handleResult(Result.from(e, position));
        }
    }

    public static CordraObject fromJsonWithPayloadsAsStrings(JsonObject obj) {
        String type = null;
        if (obj.has("type")) {
            type = obj.get("type").getAsString();
        }
        String id = null;
        if (obj.has("id")) {
            id = obj.get("id").getAsString();
        }
        CordraObject.AccessControlList acl = null;
        if (obj.has("acl")) {
            JsonElement aclEl = obj.get("acl");
            acl = gson.fromJson(aclEl, CordraObject.AccessControlList.class);
        }
        JsonObject userMetadata = null;
        if (obj.has("userMetadata")) {
            userMetadata = obj.get("userMetadata").getAsJsonObject();
        }
        JsonElement content = null;
        if (obj.has("content")) {
            content = obj.get("content");
        }
        CordraObject co = new CordraObject();
        co.id = id;
        co.type = type;
        co.content = content;
        co.acl = acl;
        co.userMetadata = userMetadata;
        Set<String> payloadNames = new HashSet<>();
        if (obj.has("payloads")) {
            List<Payload> payloads = new ArrayList<>();
            JsonArray payloadsJson = obj.get("payloads").getAsJsonArray();
            for (int i = 0; i < payloadsJson.size(); i++) {
                JsonObject payloadJson = payloadsJson.get(i).getAsJsonObject();
                Payload payload = parsePayload(payloadJson);
                payloads.add(payload);
                payloadNames.add(payload.name);
            }
            co.payloads = payloads;
        }
        if (obj.has("payloadsToDelete")) {
            JsonArray payloadsToDelete = obj.getAsJsonArray("payloadsToDelete");
            for (int i = 0; i < payloadsToDelete.size(); i++) {
                String payloadName = payloadsToDelete.get(i).getAsString();
                if (!payloadNames.contains(payloadName)) {
                    co.deletePayload(payloadName);
                }
            }
        }
        return co;
    }

    private static PayloadFromBase64String parsePayload(JsonObject payloadJson) {
        PayloadFromBase64String payload = gson.fromJson(payloadJson, PayloadFromBase64String.class);
        return payload;
    }

    private CordraObject createOrUpdate(CordraObject co) throws CordraException {
        CordraObject result;
        try {
            result = cordra.create(co, options);
        } catch (ConflictCordraException ce) {
            result = cordra.update(co, options);
        }
        return result;
    }

    //Keep for testing and possible future export tools
    public static String toJsonWithPayloadsAsBase64(CordraObject co) throws IOException {
        CordraObject clone = new CordraObject();
        clone.id = co.id;
        clone.type = co.type;
        clone.content = co.content;
        clone.userMetadata = co.userMetadata;
        clone.metadata = co.metadata;
        clone.acl = co.acl;
        clone.responseContext = co.responseContext;
        if (co.payloads != null && !co.payloads.isEmpty()) {
            List<Payload> clonePayloads = new ArrayList<>();
            for (Payload p : co.payloads) {
                PayloadFromBase64String clonePayload = new PayloadFromBase64String();
                clonePayload.name = p.name;
                clonePayload.filename = p.filename;
                clonePayload.mediaType = p.mediaType;
                clonePayload.size = p.size;
                clonePayload.setBase64String(p.getInputStream());
                clonePayloads.add(clonePayload);
            }
            clone.payloads = clonePayloads;
        }
        String result = gson.toJson(clone);
        return result;
    }
}
