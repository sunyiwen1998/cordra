package net.cnri.cordra.web.admin;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.gson.Gson;
import com.google.gson.JsonParser;
import net.cnri.cordra.*;
import net.cnri.cordra.api.*;
import net.cnri.cordra.auth.ServletAuthUtil;
import net.cnri.cordra.model.SchemaInstance;
import net.cnri.cordra.util.JacksonUtil;
import net.cnri.cordra.util.SearchUtil;
import net.cnri.cordra.web.ServletErrorUtil;
import net.cnri.cordra.web.ServletUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@WebServlet("/schemas/*")
public class SchemasServlet extends HttpServlet {
    private static Logger logger = LoggerFactory.getLogger(SchemasServlet.class);

    private InternalCordraClient internalCordra;
    private static Gson gson = GsonUtility.getGson();

    @Override
    public void init() throws ServletException {
        super.init();
        try {
            internalCordra = InternalCordraClientFactory.get();
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        String pathInfo = ServletUtil.getPath(req);
        String type = getTypeFromPathInfo(pathInfo);
        try {
            Options options = ServletAuthUtil.getOptionsFromRequest(req, resp);
            if (type == null || type.isEmpty()) {
                InitDataResponse initData = internalCordra.getInitData(options);
                Map<String, JsonNode> schemas = initData.design.schemas;
                String schemasJson = JacksonUtil.printJson(schemas);
                resp.getWriter().println(schemasJson);
            } else {
                InitDataResponse initData = internalCordra.getInitData(options);
                Map<String, JsonNode> schemas = initData.design.schemas;
                JsonNode schema = schemas.get(type);
                if (schema == null) {
                    resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
                    ServletErrorUtil.notFound(resp, "Not found: " + type);
                } else {
                    String schemaJson = JacksonUtil.printJson(schema);
                    resp.getWriter().println(schemaJson);
                }
            }
        } catch (CordraException e) {
            ServletErrorUtil.internalServerError(resp);
            logger.error("Error getting local schemas", e);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        doPut(req, resp);
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        String pathInfo = ServletUtil.getPath(req);
        String type = getTypeFromPathInfo(pathInfo);
        if ("Schema".equals(type)) {
            ServletErrorUtil.badRequest(resp, "You may not change the Schema schema.");
            return;
        }
        String schemaJson = ServletUtil.streamToString(req.getInputStream(), req.getCharacterEncoding());
        try {
            Options options = ServletAuthUtil.getOptionsFromRequest(req, resp);
            String objectId = idFromType(type, options);
            List<Payload> payloads = new ArrayList<>();
            SchemaInstance schemaInstance = new SchemaInstance();
            schemaInstance.identifier = "";
            schemaInstance.name = type;
            schemaInstance.schema = JsonParser.parseString(schemaJson);

            if (objectId == null) {
                String json = gson.toJson(schemaInstance);
                //This is create a new schema cordra object
                CordraObject schemaCordraObject = new CordraObject();
                schemaCordraObject.type = "Schema";
                schemaCordraObject.acl = null;
                schemaCordraObject.userMetadata = null;
                schemaCordraObject.payloads = payloads;
                schemaCordraObject.content = ServletUtil.getContentAsJsonElement(json);
                internalCordra.create(schemaCordraObject, options);
            } else {
                //This is update a schema cordra object using the javascript from the existing object
                CordraObject existingCordraObject = internalCordra.get(objectId, options);
                String existingJson = existingCordraObject.getContentAsString();

                SchemaInstance existingInstance = gson.fromJson(existingJson, SchemaInstance.class);
                schemaInstance.javascript = existingInstance.javascript;
                String json = gson.toJson(schemaInstance);
                existingCordraObject.content = ServletUtil.getContentAsJsonElement(json);
                internalCordra.update(existingCordraObject, options);
            }
            resp.getWriter().println("{\"msg\": \"success\"}");
        } catch (CordraException e) {
            ServletErrorUtil.internalServerError(resp);
            logger.error("Error updating schema", e);
        } catch (InvalidException e) {
            ServletErrorUtil.badRequest(resp, "InvalidException");
            logger.info("Error updating schema", e);
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        String pathInfo = ServletUtil.getPath(req);
        String type = getTypeFromPathInfo(pathInfo);
        if ("Schema".equals(type)) {
            ServletErrorUtil.badRequest(resp, "You may not delete the Schema schema. It would be bad.");
            return;
        }
        try {
            Options options = ServletAuthUtil.getOptionsFromRequest(req, resp);
            String id = idFromType(type, options);
            if (id != null) {
                internalCordra.delete(id, options);
                resp.getWriter().println("{\"msg\": \"success\"}");
            } else {
                ServletErrorUtil.badRequest(resp, "Schema " + type + " does not exist.");
            }
        } catch (CordraException e) {
            ServletErrorUtil.internalServerError(resp);
            logger.error("Error deleting schema", e);
        }
    }

    private String idFromType(String type, Options options) throws CordraException {
        String query = "+type:Schema +schemaName:\"" + SearchUtil.escape(type) + "\" -isVersion:true -objatt_isVersion:true";
        try (SearchResults<CordraObject> results = internalCordra.search(query, options)) {
            for (CordraObject co : results) {
                String name = co.content.getAsJsonObject().get("name").getAsString();
                if (type.equals(name)) {
                    return co.id;
                }
            }
        }
        return null;
    }

    private String getTypeFromPathInfo(String pathInfo) {
        if (pathInfo == null) return null;
        if (pathInfo.startsWith("/")) return pathInfo.substring(1);
        return pathInfo;
    }

}
