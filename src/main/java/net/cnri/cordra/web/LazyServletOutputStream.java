package net.cnri.cordra.web;

import java.io.IOException;
import java.io.UncheckedIOException;

import javax.servlet.ServletOutputStream;
import javax.servlet.WriteListener;

public class LazyServletOutputStream extends ServletOutputStream {
    private final IOExceptionThrowingSupplier<ServletOutputStream> delegateSupplier;
    private ServletOutputStream delegate;
    private boolean isUsed;
    private boolean writer;

    public LazyServletOutputStream(IOExceptionThrowingSupplier<ServletOutputStream> delegateSupplier) {
        this.delegateSupplier = delegateSupplier;
    }

    public boolean isUsed() {
        return isUsed;
    }

    public void notifyWriter() {
        writer = true;
    }

    private void initDelegate() throws IOException {
        if (writer) throw new IllegalStateException("WRITER");
        isUsed = true;
        if (delegate == null) delegate = delegateSupplier.get();
    }

    @Override
    public void write(int b) throws IOException {
        initDelegate();
        delegate.write(b);
    }

    @Override
    public void write(byte[] b) throws IOException {
        if (b.length == 0) return;
        initDelegate();
        delegate.write(b);
    }

    @Override
    public void write(byte[] b, int off, int len) throws IOException {
        if (len == 0) return;
        initDelegate();
        delegate.write(b, off, len);
    }

    @Override
    public void print(String s) throws IOException {
        if (s != null && s.isEmpty()) return;
        initDelegate();
        delegate.print(s);
    }

    @Override
    public void flush() throws IOException {
        initDelegate();
        delegate.flush();
    }

    @Override
    public void print(boolean b) throws IOException {
        initDelegate();
        delegate.print(b);
    }

    @Override
    public void close() throws IOException {
        initDelegate();
        delegate.close();
    }

    @Override
    public void print(char c) throws IOException {
        initDelegate();
        delegate.print(c);
    }

    @Override
    public void print(int i) throws IOException {
        initDelegate();
        delegate.print(i);
    }

    @Override
    public void print(long l) throws IOException {
        initDelegate();
        delegate.print(l);
    }

    @Override
    public void print(float f) throws IOException {
        initDelegate();
        delegate.print(f);
    }

    @Override
    public void print(double d) throws IOException {
        initDelegate();
        delegate.print(d);
    }

    @Override
    public void println() throws IOException {
        initDelegate();
        delegate.println();
    }

    @Override
    public void println(String s) throws IOException {
        initDelegate();
        delegate.println(s);
    }

    @Override
    public void println(boolean b) throws IOException {
        initDelegate();
        delegate.println(b);
    }

    @Override
    public void println(char c) throws IOException {
        initDelegate();
        delegate.println(c);
    }

    @Override
    public void println(int i) throws IOException {
        initDelegate();
        delegate.println(i);
    }

    @Override
    public void println(long l) throws IOException {
        initDelegate();
        delegate.println(l);
    }

    @Override
    public void println(float f) throws IOException {
        initDelegate();
        delegate.println(f);
    }

    @Override
    public void println(double d) throws IOException {
        initDelegate();
        delegate.println(d);
    }

    @Override
    public boolean isReady() {
        try {
            initDelegate();
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
        return delegate.isReady();
    }

    @Override
    public void setWriteListener(WriteListener writeListener) {
        try {
            initDelegate();
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
        delegate.setWriteListener(writeListener);
    }

    @FunctionalInterface
    public static interface IOExceptionThrowingSupplier<T> {
        T get() throws IOException;
    }
}
