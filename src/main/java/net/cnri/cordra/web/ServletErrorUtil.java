package net.cnri.cordra.web;

import com.google.gson.Gson;
import net.cnri.cordra.GsonUtility;
import net.cnri.cordra.api.*;
import net.cnri.cordra.model.CordraErrorResponse;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ServletErrorUtil {
    private static final Gson gson = GsonUtility.getGson();

    public static void writeCordraException(HttpServletResponse resp, CordraException e) throws IOException {
        resp.setStatus(e.getResponseCode());
        if (e.getResponse() != null) {
            resp.setContentType("application/json");
            resp.setCharacterEncoding("UTF-8");
            gson.toJson(e.getResponse(), resp.getWriter());
        }
    }

    public static void internalServerError(HttpServletResponse resp) throws IOException {
        resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        CordraErrorResponse errorResponse = new CordraErrorResponse("Something went wrong. Contact your sysadmin.");
        gson.toJson(errorResponse, resp.getWriter());
    }

    public static void badRequest(HttpServletResponse resp, String message) throws IOException {
        resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        CordraErrorResponse errorResponse = new CordraErrorResponse(message);
        gson.toJson(errorResponse, resp.getWriter());
    }

    public static void unauthorized(HttpServletResponse resp, String message) throws IOException {
        resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        CordraErrorResponse errorResponse = new CordraErrorResponse(message);
        gson.toJson(errorResponse, resp.getWriter());
    }

    public static void forbidden(HttpServletResponse resp, String message) throws IOException {
        resp.setStatus(HttpServletResponse.SC_FORBIDDEN);
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        CordraErrorResponse errorResponse = new CordraErrorResponse(message);
        gson.toJson(errorResponse, resp.getWriter());
    }

    public static void notFound(HttpServletResponse resp, String message) throws IOException {
        resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        CordraErrorResponse errorResponse = new CordraErrorResponse(message);
        gson.toJson(errorResponse, resp.getWriter());
    }
}
