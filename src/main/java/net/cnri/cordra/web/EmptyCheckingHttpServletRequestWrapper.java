package net.cnri.cordra.web;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

public class EmptyCheckingHttpServletRequestWrapper extends HttpServletRequestWrapper {

    private final boolean isEmpty;
    private final PushbackServletInputStream pushbackServletInputStream;
    private boolean streamed;
    private BufferedReader reader;

    public EmptyCheckingHttpServletRequestWrapper(HttpServletRequest request) throws IOException {
        super(request);
        this.pushbackServletInputStream = new PushbackServletInputStream(request.getInputStream());
        int ch = this.pushbackServletInputStream.read();
        if (ch < 0) {
            isEmpty = true;
        } else {
            isEmpty = false;
            this.pushbackServletInputStream.unread(ch);
        }
    }

    public static EmptyCheckingHttpServletRequestWrapper wrap(HttpServletRequest req) throws IOException {
        if (req instanceof EmptyCheckingHttpServletRequestWrapper) return (EmptyCheckingHttpServletRequestWrapper) req;
        return new EmptyCheckingHttpServletRequestWrapper(req);
    }

    public boolean isEmpty() {
        return isEmpty;
    }

    @Override
    public ServletInputStream getInputStream() throws IOException {
        if (reader != null) throw new IllegalStateException("READER");
        streamed = true;
        return pushbackServletInputStream;
    }

    @Override
    public BufferedReader getReader() throws IOException {
        if (streamed) throw new IllegalStateException("STREAMED");
        if (reader != null) return reader;
        String encoding = getCharacterEncoding();
        if (encoding == null) {
            encoding = "UTF-8";
        }
        reader = new BufferedReader(new InputStreamReader(pushbackServletInputStream, encoding));
        return reader;
    }

}
