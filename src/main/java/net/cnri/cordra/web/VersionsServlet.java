package net.cnri.cordra.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.cnri.cordra.*;
import net.cnri.cordra.api.*;
import net.cnri.cordra.auth.ServletAuthUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

@WebServlet("/versions/*")
public class VersionsServlet extends HttpServlet {
    private static final Logger logger = LoggerFactory.getLogger(VersionsServlet.class);

    private InternalCordraClient internalCordra;
    private Gson gson;

    @Override
    public void init() throws ServletException {
        super.init();
        try {
            gson = GsonUtility.getGson();
            internalCordra = InternalCordraClientFactory.get();
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    /**
     * Lists all versions of the specified object
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        String objectId = req.getParameter("objectId");
        try {
            Options options = ServletAuthUtil.getOptionsFromRequest(req, resp);
            List<VersionInfo> versionInfos = internalCordra.getVersionsFor(objectId, options);
            String json = gson.toJson(versionInfos);
            PrintWriter w = resp.getWriter();
            w.write(json);
            w.close();
        } catch (InternalErrorCordraException e) {
            logger.error("Unexpected error calling get versions", e);
            ServletErrorUtil.internalServerError(resp);
        } catch (CordraException e) {
            ServletErrorUtil.writeCordraException(resp, e);
        } catch (Exception e) {
            logger.error("Unexpected error calling get versions", e);
            ServletErrorUtil.internalServerError(resp);
        }
    }

    /**
     * Creates a new locked copy of the specified object and returns the new Id.
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        String objectId = req.getParameter("objectId");
        String versionId = req.getParameter("versionId");
        boolean clonePayloads = ServletUtil.getBooleanParameter(req, "clonePayloads", true);
        if (objectId != null) {
            try {
                Options options = ServletAuthUtil.getOptionsFromRequest(req, resp);
                VersionInfo versionInfo = internalCordra.publishVersion(objectId, versionId, clonePayloads, options);
                String json = gson.toJson(versionInfo);
                PrintWriter w = resp.getWriter();
                w.write(json);
                w.close();
            } catch (InternalErrorCordraException e) {
                logger.error("Unexpected error calling get versions", e);
                ServletErrorUtil.internalServerError(resp);
            } catch (CordraException e) {
                ServletErrorUtil.writeCordraException(resp, e);
            } catch (Exception e) {
                logger.error("Unexpected error calling get versions", e);
                ServletErrorUtil.internalServerError(resp);
            }
        }
    }
}
