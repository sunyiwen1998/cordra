package net.cnri.cordra.web.doip;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import net.cnri.cordra.util.HttpUtil;
import net.dona.doip.DoipConstants;
import net.dona.doip.OutDoipMessage;
import net.dona.doip.server.DoipServerResponse;
import net.dona.doip.util.GsonUtility;

import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

public class DoipServerHttpResponse implements DoipServerResponse {

    private final OutDoipMessageHttpResponse outDoipMessageHttp;
    private final String requestId;
    private String status = DoipConstants.STATUS_OK;
    private JsonObject attributes;
    private boolean wroteCompactOutput;
    private boolean committed;

    public static Map<String, Integer> statusMap = new HashMap<>();
    static {
        statusMap.put(DoipConstants.STATUS_OK, 200);
        statusMap.put(DoipConstants.STATUS_BAD_REQUEST, 400);
        statusMap.put(DoipConstants.STATUS_UNAUTHENTICATED, 401);
        statusMap.put(DoipConstants.STATUS_FORBIDDEN, 403);
        statusMap.put(DoipConstants.STATUS_NOT_FOUND, 404);
        statusMap.put(DoipConstants.STATUS_CONFLICT, 409);
        statusMap.put(DoipConstants.STATUS_DECLINED, 400);
        statusMap.put(DoipConstants.STATUS_ERROR, 500);
    }

    public DoipServerHttpResponse(String requestId, OutDoipMessageHttpResponse outDoipMessageHttp) {
        this.requestId = requestId;
        this.outDoipMessageHttp = outDoipMessageHttp;
    }

    protected void writeInitialSegment(JsonElement output) throws IOException {
        int httpStatus = doipStatusToHttpStatus(status);
        outDoipMessageHttp.setStatus(httpStatus);
        if (attributes != null) {
            if (attributes.has("mediaType")) {
                String contentType = attributes.get("mediaType").getAsString();
                outDoipMessageHttp.setContentType(contentType);
            }
            if (attributes.has("filename")) {
                String filename = attributes.get("filename").getAsString();
                String disposition = "inline";
                String contentDisposition = HttpUtil.contentDispositionHeaderFor(disposition, filename);
                outDoipMessageHttp.setContentDisposition(contentDisposition);
            }
        }
        JsonObject doipResponseHeader = new JsonObject();
        doipResponseHeader.addProperty("status", status);
        if (attributes != null) {
            doipResponseHeader.add("attributes", attributes);
        }
        if (requestId != null) {
            doipResponseHeader.addProperty("requestId", requestId);
        }
        outDoipMessageHttp.setDoipResponseHeader(doipResponseHeader);
        if (output != null) {
            try (Writer writer = outDoipMessageHttp.getJsonWriter()) {
                GsonUtility.getGson().toJson(output, writer);
            } catch (JsonParseException e) {
                throw new IOException("Error writing initial segment", e);
            }
        }
    }

    private int doipStatusToHttpStatus(String doipStatus) {
        Integer httpStatus = statusMap.get(doipStatus);
        if (httpStatus == null) return 200;
        return httpStatus.intValue();
    }

    public String getRequestId() {
        return requestId;
    }

    public String getStatus() {
        return status;
    }

    public JsonObject getAttributes() {
        return attributes;
    }

    @Override
    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public void setAttribute(String key, JsonElement value) {
        if (this.attributes == null) this.attributes = new JsonObject();
        this.attributes.add(key, value);
    }

    @Override
    public void setAttribute(String key, String value) {
        if (this.attributes == null) this.attributes = new JsonObject();
        this.attributes.addProperty(key, value);
    }

    @Override
    public void setAttributes(JsonObject attributes) {
        this.attributes = attributes;
    }

    @Override
    public void commit() throws IOException {
        if (wroteCompactOutput || committed) return;
        committed = true;
        writeInitialSegment(null);
    }

    @Override
    public void writeCompactOutput(JsonElement output) throws IOException {
        if (wroteCompactOutput) throw new IllegalStateException("already wrote compact output");
        if (committed) throw new IllegalStateException("already committed");
        wroteCompactOutput = true;
        writeInitialSegment(output);
        outDoipMessageHttp.close();
    }

    @Override
    public OutDoipMessage getOutput() throws IOException {
        if (wroteCompactOutput) throw new IllegalStateException("already wrote compact output");
        if (!committed) {
            committed = true;
            writeInitialSegment(null);
        }
        return outDoipMessageHttp;
    }
}
