package net.cnri.cordra;

import net.cnri.cordra.indexer.IndexerConfig;
import net.cnri.cordra.model.CordraConfig;
import net.cnri.cordra.storage.StorageConfig;
import net.cnri.cordra.sync.curator.CuratorResources;
import org.apache.curator.framework.CuratorFramework;
import org.apache.zookeeper.KeeperException;
import javax.servlet.ServletContext;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class CordraConfigSource {
    private static final String ZK_CONFIG_PATH = "/config.json";
    private static CordraConfig cordraConfig = null;

    public synchronized static CordraConfig getConfig(ServletContext context) throws IOException {
        if (cordraConfig != null) {
            return cordraConfig;
        }
        CordraConfig config = findAndGetConfigFromZooKeeper(context);
        if (config == null) config = getConfigFromContextAttribute(context);
        if (config == null) config = getConfigFromWar(context);
        if (config == null) config = getConfigFromPropertyOrEnv(context);
        if (config == null) config = getConfigFromPathInPropertyOrEnv(context);
        if (config == null) config = getConfigFromCordraData();
        cordraConfig = getDefaultConfigIfNull(config);
        return cordraConfig;
    }

    public static CordraConfig getConfigForTesting(String zookeeperConnectionString) throws Exception {
        if (zookeeperConnectionString != null) {
            return getConfigFromZooKeeper(zookeeperConnectionString, null);
        } else {
            return getDefaultConfigIfNull(null);
        }
    }

    public static CordraConfig getDefaultConfigIfNull(CordraConfig config) {
        if (config == null) config = CordraConfig.getNewDefaultInstance();
        if (config.index == null) {
            config.index = IndexerConfig.getNewDefaultInstance();
        }
        if (config.storage == null) {
            config.storage = StorageConfig.getNewDefaultInstance();
        }
        return config;
    }

    private static CordraConfig findAndGetConfigFromZooKeeper(ServletContext context) {
        CordraConfig config = null;
        String zookeeperConnectionString = getZkConnectionString(context);
        if (zookeeperConnectionString != null) {
            String configName = getZkConfigName(context);
            try {
                config = getConfigFromZooKeeper(zookeeperConnectionString, configName);
            } catch (Exception e) {
                // no-op, continue on
            }
        }
        return config;
    }

    private static String getZkConfigName(ServletContext context) {
        return getPropertyFromSomewhere(context, "cordra.configName", "cordra_config_name", "configName", "config_name");
    }

    public static String getZkConnectionString(ServletContext context) {
        return getPropertyFromSomewhere(context,
                "cordra.zookeeperConnectionString", "cordra_zookeeper_connection_string",
                "zookeeperConnectionString", "zookeeper_connection_string");
    }

    private static String getPropertyFromSomewhere(ServletContext context, String... names) {
        if (context != null) {
            for (String name : names) {
                String value = context.getInitParameter(name);
                if (value != null) return value;
            }
        }
        for (String name : names) {
            String value = System.getProperty(name);
            if (value == null) value = System.getenv(name);
            if (value != null) return value;
        }
        return null;
    }

    private static CordraConfig getConfigFromCordraData() throws IOException {
        CordraConfig config = null;
        String dataDir = System.getProperty(Constants.CORDRA_DATA);
        if (dataDir == null) dataDir = System.getenv(Constants.CORDRA_DATA);
        if (dataDir == null) {
            String dataWithUnderscores = Constants.CORDRA_DATA.replace(".", "_");
            dataDir = System.getProperty(dataWithUnderscores);
            if (dataDir == null) dataDir = System.getenv(dataWithUnderscores);
        }
        if (dataDir != null) {
            Path path = Paths.get(dataDir).resolve("config.json");
            if (Files.exists(path)) {
                try (Reader in = Files.newBufferedReader(path)) {
                    config = GsonUtility.getGson().fromJson(in, CordraConfig.class);
                }
            }
        }
        return config;
    }

    private static CordraConfig getConfigFromPathInPropertyOrEnv(ServletContext context) throws IOException {
        CordraConfig config = null;
        String configJsonPath = getPropertyFromSomewhere(context,
            "cordra.config.json.path", "cordra_config_json_path",
            "config.json.path", "config_json_path");
        if (configJsonPath != null) {
            try (Reader in = Files.newBufferedReader(Paths.get(configJsonPath))) {
                config = GsonUtility.getGson().fromJson(in, CordraConfig.class);
            }
        }
        return config;
    }

    private static CordraConfig getConfigFromPropertyOrEnv(ServletContext context) {
        CordraConfig config = null;
        String configJson = getPropertyFromSomewhere(context,
            "cordra.config.json", "cordra_config_json",
            "config.json", "config_json");
        if (configJson != null) {
            config = GsonUtility.getGson().fromJson(configJson, CordraConfig.class);
        }
        return config;
    }

    private static CordraConfig getConfigFromWar(ServletContext context) throws IOException {
        CordraConfig config = null;
        try (InputStream in = context.getResourceAsStream("/WEB-INF/config.json")) {
            if (in != null) {
                try (InputStreamReader isr = new InputStreamReader(in, StandardCharsets.UTF_8)) {
                    config = GsonUtility.getGson().fromJson(isr, CordraConfig.class);
                }
            }
        }
        return config;
    }

    private static CordraConfig getConfigFromContextAttribute(ServletContext context) {
        CordraConfig config = null;
        String configJson = (String) context.getAttribute("config.json");
        if (configJson == null) configJson = (String) context.getAttribute("config_json");
        if (configJson != null) {
            config = GsonUtility.getGson().fromJson(configJson, CordraConfig.class);
        }
        return config;
    }

    private static CordraConfig getConfigFromZooKeeper(String zookeeperConnectionString, String configName) throws Exception {
        CordraConfig config;
        try (CuratorFramework client = CuratorResources.getClient(zookeeperConnectionString)) {
            client.start();
            if (configName == null || configName.isEmpty()) configName = ZK_CONFIG_PATH;
            if (!configName.startsWith("/")) configName = "/" + configName;
            try {
                byte[] configBytes = client.getData().forPath(configName);
                config = GsonUtility.getGson().fromJson(new String(configBytes, StandardCharsets.UTF_8), CordraConfig.class);
            } catch (KeeperException.NoNodeException e) {
                config = null;
            }
        }
        return config;
    }
}
