package net.cnri.cordra.api;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.concurrent.CountDownLatch;

import net.cnri.cordra.util.HttpUtil;

public class SimpleCallResponseHandler implements CallResponseHandler {

    private final CountDownLatch commitLatch;
    private final OutputStream outputStream;
    private boolean gotOutputStream;
    private Writer writer;

    private String mediaType;
    private String filename;

    public SimpleCallResponseHandler(OutputStream outputStream) {
        this.outputStream = new CommittingOutputStream(outputStream);
        this.commitLatch = new CountDownLatch(1);
    }

    public void awaitCommit() throws InterruptedException {
        this.commitLatch.await();
    }

    public void commit() {
        this.commitLatch.countDown();
    }

    @Override
    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    @Override
    public void setFilename(String filename) {
        this.filename = filename;
    }

    @Override
    public void setRange(boolean partial, boolean unsatisfied, Long start, Long end, Long size) {
        // currently ignored (as not return by CordraClient methods)
    }

    @Override
    public void setLength(long length) {
        // currently ignored (as not return by CordraClient methods)
    }

    public String getMediaType() {
        return mediaType;
    }

    public String getFilename() {
        return filename;
    }

    @Override
    public OutputStream getOutputStream() throws IOException {
        if (writer != null) throw new IllegalStateException("Cannot use both getOutputStream and getWriter");
        gotOutputStream = true;
        return outputStream;
    }

    @Override
    public Writer getWriter() throws IOException {
        if (writer != null) return writer;
        if (gotOutputStream) throw new IllegalStateException("Cannot use both getOutputStream and getWriter");
        String charset = HttpUtil.getCharset(mediaType);
        writer = new OutputStreamWriter(outputStream, charset);
        return writer;
    }

    public void flush() throws IOException {
        if (writer != null) writer.flush();
    }

    private class CommittingOutputStream extends FilterOutputStream {

        public CommittingOutputStream(OutputStream out) {
            super(out);
        }

        @Override
        public void write(int b) throws IOException {
            commit();
            super.write(b);
        }

        @Override
        public void write(byte[] b) throws IOException {
            if (b.length > 0) commit();
            super.write(b);
        }

        @Override
        public void write(byte[] b, int off, int len) throws IOException {
            if (len > 0) commit();
            super.write(b, off, len);
        }

        @Override
        public void close() throws IOException {
            commit();
            super.close();
        }


    }

}
