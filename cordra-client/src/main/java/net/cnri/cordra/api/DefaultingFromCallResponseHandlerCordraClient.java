package net.cnri.cordra.api;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicBoolean;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import net.cnri.cordra.util.DelegatedCloseableInputStream;
import net.cnri.util.StreamUtil;

public interface DefaultingFromCallResponseHandlerCordraClient extends CallResponseHandlerEnabledCordraClient {

    @FunctionalInterface
    static interface ThrowingCallResponseHandlerConsumer {
        void consume(CallResponseHandler handler) throws CordraException;
    }

    ExecutorService getCallResponseHandlerExecutorService();

    @SuppressWarnings("resource")
    default CallResponse buildCallResponse(ThrowingCallResponseHandlerConsumer consumer) throws CordraException {
        PipedOutputStream out = new PipedOutputStream();
        PipedInputStream pipedInputStream;
        try {
            pipedInputStream = new PipedInputStream(out);
        } catch (IOException e) {
            throw new InternalErrorCordraException(e);
        }
        SimpleCallResponseHandler handler = new SimpleCallResponseHandler(out);
        AtomicBoolean exceptionThrown = new AtomicBoolean();
        Future<Void> future = getCallResponseHandlerExecutorService().submit(() -> {
            boolean finishedNormally = false;
            try {
                try {
                    consumer.consume(handler);
                    finishedNormally = true;
                    return null;
                } finally {
                    handler.flush();
                    out.close();
                }
            } catch (Throwable t) {
                // flag exceptions thrown in consumer.consume(handler) in order to throw before returning a CallResponse
                // (later I/O exceptions will be thrown when the CallResponse is closed instead)
                if (!finishedNormally) exceptionThrown.set(true);
                throw t;
            } finally {
                handler.commit();
            }
        });
        try {
            handler.awaitCommit();
        } catch (InterruptedException e) {
            throw new InternalErrorCordraException(e);
        }
        if (exceptionThrown.get()) {
            try {
                pipedInputStream.close();
            } catch (Exception e) {
                // ignore
            }
            try {
                future.get();
            } catch (ExecutionException e) {
                if (e.getCause() instanceof CordraException) throw (CordraException) e.getCause();
                if (e.getCause() instanceof RuntimeException) throw (RuntimeException) e.getCause();
                throw new InternalErrorCordraException(e);
            } catch (InterruptedException e) {
                throw new InternalErrorCordraException(e);
            }
            throw new InternalErrorCordraException("Unexpected exception in call response handling");
        }
        InputStream in = new DelegatedCloseableInputStream(pipedInputStream, () -> {
            try {
                future.get();
            } catch (ExecutionException e) {
                if (e.getCause() instanceof CordraException) throw new UncheckedCordraException((CordraException) e.getCause());
                if (e.getCause() instanceof RuntimeException) throw (RuntimeException) e.getCause();
                throw new UncheckedCordraException(new InternalErrorCordraException(e));
            } catch (InterruptedException e) {
                throw new UncheckedCordraException(new InternalErrorCordraException(e));
            }
        });
        CallResponse res = new CallResponse();
        res.headers.mediaType = handler.getMediaType();
        res.headers.filename = handler.getFilename();
        res.body = in;
        return res;
    }

    @Override
    @SuppressWarnings("resource")
    default InputStream getPayload(String id, String payloadName, Options options) throws CordraException {
        return getPayloadAsResponse(id, payloadName, options).body;
    }

    @Override
    default CallResponse getPayloadAsResponse(String id, String payloadName, Options options) throws CordraException {
        return buildCallResponse(handler -> getPayload(id, payloadName, handler, options));
    }

    @Override
    default void getPayload(String id, String payloadName, CallResponseHandler handler, Options options) throws CordraException {
        getPartialPayload(id, payloadName, null, null, handler, options);
    }

    @Override
    @SuppressWarnings("resource")
    default InputStream getPartialPayload(String id, String payloadName, Long start, Long end, Options options) throws CordraException {
        return getPartialPayloadAsResponse(id, payloadName, start, end, options).body;
    }

    @Override
    default CallResponse getPartialPayloadAsResponse(String id, String payloadName, Long start, Long end, Options options) throws CordraException {
        return buildCallResponse(handler -> getPartialPayload(id, payloadName, start, end, handler, options));
    }

    @Override
    void getPartialPayload(String id, String payloadName, Long start, Long end, CallResponseHandler handler, Options options) throws CordraException;

    @Override
    default JsonElement call(String objectId, String methodName, JsonElement params, Options options) throws CordraException {
        try (
                CallResponse callResponse = callAsResponse(objectId, methodName, params, options);
                InputStreamReader reader = new InputStreamReader(callResponse.body, StandardCharsets.UTF_8);
                ) {
            String response = StreamUtil.readFully(reader);
            if (response.isEmpty()) return null;
            return JsonParser.parseString(response);
        } catch (IOException e) {
            throw new InternalErrorCordraException(e);
        }
    }

    @Override
    default CallResponse callAsResponse(String objectId, String methodName, JsonElement params, Options options) throws CordraException {
        return buildCallResponse(handler -> call(objectId, methodName, params, handler, options));
    }

    @Override
    default void call(String objectId, String methodName, JsonElement params, CallResponseHandler handler, Options options) throws CordraException {
        ByteArrayInputStream input = params == null ? null : new ByteArrayInputStream(params.toString().getBytes(StandardCharsets.UTF_8));
        call(objectId, methodName, input, handler, options);
    }

    @Override
    default JsonElement call(String objectId, String methodName, InputStream input, Options options) throws CordraException {
        try (
                CallResponse callResponse = callAsResponse(objectId, methodName, input, options);
                InputStreamReader reader = new InputStreamReader(callResponse.body, StandardCharsets.UTF_8);
                ) {
            String response = StreamUtil.readFully(reader);
            if (response.isEmpty()) return null;
            return JsonParser.parseString(response);
        } catch (IOException e) {
            throw new InternalErrorCordraException(e);
        }
    }

    @Override
    default CallResponse callAsResponse(String objectId, String methodName, InputStream input, Options options) throws CordraException {
        return buildCallResponse(handler -> call(objectId, methodName, input, handler, options));
    }

    @Override
    void call(String objectId, String methodName, InputStream input, CallResponseHandler handler, Options options) throws CordraException;

    @Override
    default JsonElement callForType(String type, String methodName, JsonElement params, Options options) throws CordraException {
        try (
                CallResponse callResponse = callForTypeAsResponse(type, methodName, params, options);
                InputStreamReader reader = new InputStreamReader(callResponse.body, StandardCharsets.UTF_8);
                ) {
            String response = StreamUtil.readFully(reader);
            if (response.isEmpty()) return null;
            return JsonParser.parseString(response);
        } catch (IOException e) {
            throw new InternalErrorCordraException(e);
        }
    }

    @Override
    default CallResponse callForTypeAsResponse(String type, String methodName, JsonElement params, Options options) throws CordraException {
        return buildCallResponse(handler -> callForType(type, methodName, params, handler, options));
    }

    @Override
    default void callForType(String type, String methodName, JsonElement params, CallResponseHandler handler, Options options) throws CordraException {
        ByteArrayInputStream input = params == null ? null : new ByteArrayInputStream(params.toString().getBytes(StandardCharsets.UTF_8));
        callForType(type, methodName, input, handler, options);
    }

    @Override
    default JsonElement callForType(String type, String methodName, InputStream input, Options options) throws CordraException {
        try (
                CallResponse callResponse = callForTypeAsResponse(type, methodName, input, options);
                InputStreamReader reader = new InputStreamReader(callResponse.body, StandardCharsets.UTF_8);
                ) {
            String response = StreamUtil.readFully(reader);
            if (response.isEmpty()) return null;
            return JsonParser.parseString(response);
        } catch (IOException e) {
            throw new InternalErrorCordraException(e);
        }
    }

    @Override
    default CallResponse callForTypeAsResponse(String type, String methodName, InputStream input, Options options) throws CordraException {
        return buildCallResponse(handler -> callForType(type, methodName, input, handler, options));
    }

    @Override
    void callForType(String type, String methodName, InputStream input, CallResponseHandler handler, Options options) throws CordraException;
}
