package net.cnri.cordra.doip;

import java.io.IOException;
import java.security.PublicKey;
import java.security.cert.X509Certificate;

import net.cnri.cordra.api.Options;
import net.dona.doip.InDoipMessage;
import net.dona.doip.server.DoipServerRequestImpl;

public class DoipServerRequestImplWithCachedOptions extends DoipServerRequestImpl {

    private Options options;

    public DoipServerRequestImplWithCachedOptions(InDoipMessage inDoipMessage, String clientCertId, PublicKey clientCertPublicKey, X509Certificate[] clientCertChain) throws IOException {
        super(inDoipMessage, clientCertId, clientCertPublicKey, clientCertChain);
    }

    public Options getOptions() {
        return options;
    }

    public void setOptions(Options options) {
        this.options = options;
    }
}
