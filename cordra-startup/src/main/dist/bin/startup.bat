@echo off

REM Get the full name of the directory where the Cordra code is installed
set PRG=%~dp0%
set CORDRADIR=%PRG%..

set CP=%CORDRADIR%\sw\lib\*;%CORDRADIR%\sw\lib\cordra-client\*;%CORDRADIR%\sw\lib\servletContainer\*;%CORDRADIR%\data\lib\*

java -cp "%CP%" net.cnri.cordra.startup.CordraStartup --data %CORDRADIR%\data --webapps-priority %CORDRADIR%\sw\webapps-priority
