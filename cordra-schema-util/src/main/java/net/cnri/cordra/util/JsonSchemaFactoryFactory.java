package net.cnri.cordra.util;

import com.github.fge.jackson.NodeType;
import com.github.fge.jsonschema.cfg.ValidationConfiguration;
import com.github.fge.jsonschema.cfg.ValidationConfigurationBuilder;
import com.github.fge.jsonschema.core.keyword.syntax.checkers.helpers.TypeOnlySyntaxChecker;
import com.github.fge.jsonschema.core.load.configuration.LoadingConfigurationBuilder;
import com.github.fge.jsonschema.library.DraftV4Library;
import com.github.fge.jsonschema.library.Keyword;
import com.github.fge.jsonschema.library.KeywordBuilder;
import com.github.fge.jsonschema.library.LibraryBuilder;
import com.github.fge.jsonschema.main.JsonSchemaFactory;

import net.cnri.cordra.schema.SchemaResolver;
import net.cnri.cordra.schema.fge.DraftV4FgeJsonSchemaFactoryFactory;
import net.cnri.cordra.schema.fge.FgeJsonSchemaUtil;
import net.cnri.cordra.schema.networknt.JsonPrunerJsonSchemaFactory;

/**
 * A modification of fge json-schema-validator to allow determining extra properties for {@link JsonPruner}, and
 * also to ignore the {@code $schema} keyword.
 *
 * @deprecated Prefer {@link JsonPrunerJsonSchemaFactory} or {@link DraftV4FgeJsonSchemaFactoryFactory}.
 */
@Deprecated
public class JsonSchemaFactoryFactory {
    private static JsonSchemaFactory INSTANCE;

    static {
        LibraryBuilder lib = DraftV4Library.get().thaw();
        {
            KeywordBuilder kb = Keyword.newBuilder("additionalProperties");
            kb.withIdentityDigester(NodeType.OBJECT);
            kb.withSyntaxChecker(new TypeOnlySyntaxChecker("additionalProperties", NodeType.BOOLEAN, NodeType.OBJECT));
            kb.withValidatorClass(AdditionalPropertiesValidator.AdditionalProperties.class);
            lib.addKeyword(kb.freeze());
        }
        {
            KeywordBuilder kb = Keyword.newBuilder("properties");
            kb.withIdentityDigester(NodeType.OBJECT);
            kb.withSyntaxChecker(new TypeOnlySyntaxChecker("properties", NodeType.BOOLEAN, NodeType.OBJECT));
            kb.withValidatorClass(AdditionalPropertiesValidator.Properties.class);
            lib.addKeyword(kb.freeze());
        }
        {
            KeywordBuilder kb = Keyword.newBuilder("patternProperties");
            kb.withIdentityDigester(NodeType.OBJECT);
            kb.withSyntaxChecker(new TypeOnlySyntaxChecker("patternProperties", NodeType.BOOLEAN, NodeType.OBJECT));
            kb.withValidatorClass(AdditionalPropertiesValidator.PatternProperties.class);
            lib.addKeyword(kb.freeze());
        }
        ValidationConfigurationBuilder vcb = ValidationConfiguration.newBuilder();
        FgeJsonSchemaUtil.clearLibraries(vcb);
        vcb.setDefaultLibrary("https://cordra.org/", lib.freeze());
        LoadingConfigurationBuilder lcb = FgeJsonSchemaUtil.buildRefAwareLoadingConfigurationBuilder();
        INSTANCE = JsonSchemaFactory.newBuilder()
            .setValidationConfiguration(vcb.freeze())
            .setLoadingConfiguration(lcb.freeze())
            .freeze();
    }

    public static JsonSchemaFactory newJsonSchemaFactory() {
        return newJsonSchemaFactory(null);
    }

    public static JsonSchemaFactory newJsonSchemaFactory(SchemaResolver resolver) {
        JsonSchemaFactory res = JsonSchemaFactoryFactory.INSTANCE.thaw().freeze();
        FgeJsonSchemaUtil.setSchemaResolver(res, resolver);
        return res;
    }

}
