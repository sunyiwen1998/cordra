package net.cnri.cordra.schema;

import com.fasterxml.jackson.databind.JsonNode;
import net.cnri.cordra.InvalidException;

public class LazySchema<T> {

    private final boolean fixedSchema;
    private final GenerationalSchemaFactory<T> factory;
    private final JsonNode schemaNode;
    private final String baseUri;

    private volatile long generation = -1;
    private T schema;

    public LazySchema(JsonNode schemaNode, String baseUri, GenerationalSchemaFactory<T> factory) {
        this.schemaNode = schemaNode;
        this.baseUri = baseUri;
        this.fixedSchema = false;
        this.factory = factory;
    }

    public LazySchema(JsonNode schemaNode, String baseUri, T schema) {
        this.schemaNode = schemaNode;
        this.baseUri = baseUri;
        this.fixedSchema = true;
        this.factory = null;
        this.schema = schema;
    }

    public JsonNode getSchemaNode() {
        return schemaNode;
    }

    public String getBaseUri() {
        return baseUri;
    }

    public T getSchema() throws InvalidException {
        if (fixedSchema) return schema;
        if (generation < factory.getGeneration()) {
            updateCachedSchema();
        }
        return schema;
    }

    private synchronized void updateCachedSchema() throws InvalidException {
        // double-checked locking
        if (generation >= factory.getGeneration()) return;
        GenerationalSchemaFactory.SchemaAndGeneration<T> schemaAndGeneration = factory.getSchema(schemaNode, baseUri);
        schema = schemaAndGeneration.schema;
        generation = schemaAndGeneration.generation;
    }
}
