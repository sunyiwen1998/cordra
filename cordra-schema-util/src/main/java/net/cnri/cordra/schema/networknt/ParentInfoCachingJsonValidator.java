package net.cnri.cordra.schema.networknt;

import com.fasterxml.jackson.databind.JsonNode;
import com.networknt.schema.JsonValidator;
import com.networknt.schema.ValidationMessage;

import java.util.Set;

public class ParentInfoCachingJsonValidator implements JsonValidator {

    private final JsonValidator delegate;

    public ParentInfoCachingJsonValidator(JsonValidator delegate) {
        this.delegate = delegate;
    }

    @Override
    public Set<ValidationMessage> validate(JsonNode rootNode) {
        return validate(rootNode, rootNode, JsonValidator.AT_ROOT);
    }

    @Override
    public Set<ValidationMessage> validate(JsonNode node, JsonNode rootNode, String at) {
        JsonNode parentJsonNode = ParentInfoCachingUtil.parentJsonNodeThreadLocal.get();
        String parentAt = ParentInfoCachingUtil.parentAtThreadLocal.get();
        String parentJsonPointer = ParentInfoCachingUtil.parentJsonPointerThreadLocal.get();

        String lastSegment = ParentInfoCachingUtil.determineLastSegment(at);
        String jsonPointer = ParentInfoCachingUtil.determineJsonPointer(parentJsonPointer, lastSegment);

        ParentInfoCachingUtil.parentJsonNodeThreadLocal.set(node);
        ParentInfoCachingUtil.parentAtThreadLocal.set(at);
        ParentInfoCachingUtil.parentJsonPointerThreadLocal.set(jsonPointer);
        try {
            return delegate.validate(node, rootNode, at);
        } finally {
            ParentInfoCachingUtil.parentJsonNodeThreadLocal.set(parentJsonNode);
            ParentInfoCachingUtil.parentAtThreadLocal.set(parentAt);
            ParentInfoCachingUtil.parentJsonPointerThreadLocal.set(parentJsonPointer);
        }
    }

    @Override
    public Set<ValidationMessage> walk(JsonNode node, JsonNode rootNode, String at, boolean shouldValidateSchema) {
        throw new UnsupportedOperationException();
    }
}
