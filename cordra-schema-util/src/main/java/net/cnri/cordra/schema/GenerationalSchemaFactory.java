package net.cnri.cordra.schema;

import com.fasterxml.jackson.databind.JsonNode;

import net.cnri.cordra.InvalidException;

public interface GenerationalSchemaFactory<T> {
    long incrementAndGetGeneration();
    long getGeneration();
    SchemaAndGeneration<T> getSchema(JsonNode schemaNode, String baseUri) throws InvalidException;

    class SchemaAndGeneration<T> {
        public final T schema;
        public final long generation;

        public SchemaAndGeneration(T schema, long generation) {
            this.schema = schema;
            this.generation = generation;
        }
    }
}
