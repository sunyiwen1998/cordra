package net.cnri.cordra.schema.networknt;

import java.lang.reflect.Field;
import java.net.URI;
import java.util.AbstractMap;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import com.fasterxml.jackson.databind.JsonNode;
import com.networknt.schema.BaseJsonValidator;
import com.networknt.schema.JsonSchema;
import com.networknt.schema.JsonSchemaException;
import com.networknt.schema.JsonSchemaFactory;
import net.cnri.cordra.InvalidException;
import net.cnri.cordra.schema.SchemaResolver;
import net.cnri.cordra.schema.SchemaUtil;

public class NetworkntJsonSchemaUtil {

    private static final URI FILE_URI = URI.create("file:/");

    private static class SchemaResolvingPseudoMap extends AbstractMap<URI, JsonSchema> implements ConcurrentMap<URI, JsonSchema> {

        private final ConcurrentMap<URI, JsonSchema> actualCache = new ConcurrentHashMap<>();
        private final JsonSchemaFactory factory;
        private final SchemaResolver resolver;

        public SchemaResolvingPseudoMap(JsonSchemaFactory factory, SchemaResolver resolver) {
            this.factory = factory;
            this.resolver = resolver;
        }

        @Override
        public boolean containsKey(Object key) {
            return true;
        }

        @Override
        public JsonSchema get(Object key) {
            URI uri = (URI)key;
            JsonSchema res = actualCache.get(uri);
            if (res != null) return res;
            JsonNode node = SchemaUtil.loadUsingResolver(uri, resolver);
            if (node == null) throw new JsonSchemaException("Failed to load JSON schema " + key + "!");
            JsonSchema schema = getJsonSchema(factory, node);
            actualCache.put(uri, schema);
            return schema;
        }

        @Override
        public void clear() {
            actualCache.clear();
        }

        @Override
        public JsonSchema putIfAbsent(URI key, JsonSchema value) {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean remove(Object key, Object value) {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean replace(URI key, JsonSchema oldValue, JsonSchema newValue) {
            throw new UnsupportedOperationException();
        }

        @Override
        public JsonSchema replace(URI key, JsonSchema value) {
            throw new UnsupportedOperationException();
        }

        @Override
        public Set<Entry<URI, JsonSchema>> entrySet() {
            throw new UnsupportedOperationException();
        }
    }

    public static void setSchemaResolver(JsonSchemaFactory factory, SchemaResolver resolver) {
        ConcurrentMap<URI, JsonSchema> resolvingCache = new SchemaResolvingPseudoMap(factory, resolver);
        try {
            Field uriSchemaCacheField = JsonSchemaFactory.class.getDeclaredField("uriSchemaCache");
            uriSchemaCacheField.setAccessible(true);
            uriSchemaCacheField.set(factory, resolvingCache);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new AssertionError(e);
        }
    }

    public static void clearCache(JsonSchemaFactory factory) {
        try {
            Field uriSchemaCacheField = JsonSchemaFactory.class.getDeclaredField("uriSchemaCache");
            uriSchemaCacheField.setAccessible(true);
            ((ConcurrentMap<?,?>) uriSchemaCacheField.get(factory)).clear();
        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new AssertionError(e);
        }
    }

    private static void fixJsonSchema(JsonSchema schema) {
        // A strangeness from networknt/json-schema-validator that might interact badly with our $ref resolver
        try {
            Field suppressSubSchemaRetrievalField = BaseJsonValidator.class.getDeclaredField("suppressSubSchemaRetrieval");
            suppressSubSchemaRetrievalField.setAccessible(true);
            suppressSubSchemaRetrievalField.setBoolean(schema, true);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new AssertionError(e);
        }
    }

    private static JsonSchema getJsonSchema(JsonSchemaFactory factory, JsonNode node) throws JsonSchemaException {
        node = SchemaUtil.schemaWithoutDollarSchema(node);
        JsonSchema schema = factory.getSchema(FILE_URI, node);
        fixJsonSchema(schema);
        return schema;
    }

    public static JsonSchema getJsonSchema(JsonSchemaFactory factory, JsonNode node, String baseUri) throws InvalidException {
        node = SchemaUtil.schemaWithBaseUri(node, baseUri);
        try {
            return getJsonSchema(factory, node);
        } catch (JsonSchemaException e) {
            throw new InvalidException("Invalid schema", e);
        }
    }

}
