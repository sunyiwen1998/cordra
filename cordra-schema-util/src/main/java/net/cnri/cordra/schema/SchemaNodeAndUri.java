package net.cnri.cordra.schema;

import com.fasterxml.jackson.databind.JsonNode;

public class SchemaNodeAndUri {
    private final JsonNode schema;
    private final String baseUri;

    public SchemaNodeAndUri(JsonNode schema, String baseUri) {
        this.schema = schema;
        this.baseUri = baseUri;
    }

    public JsonNode getSchemaNode() {
        return schema;
    }

    public String getBaseUri() {
        return baseUri;
    }
}