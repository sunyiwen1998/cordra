package net.cnri.cordra.schema.networknt;

import java.lang.reflect.Constructor;

import com.fasterxml.jackson.databind.JsonNode;
import com.networknt.schema.JsonSchema;
import com.networknt.schema.JsonValidator;
import com.networknt.schema.Keyword;
import com.networknt.schema.ValidationContext;

public class JsonPrunerReplacingKeyword implements Keyword {

    private final Keyword delegate;
    private final Class<? extends JsonValidator> validator;

    public JsonPrunerReplacingKeyword(Keyword delegate, Class<? extends JsonValidator> validator) {
        this.delegate = delegate;
        this.validator = validator;
    }

    @Override
    public String getValue() {
        return delegate.getValue();
    }

    @Override
    public JsonValidator newValidator(String schemaPath, JsonNode schemaNode, JsonSchema parentSchema, ValidationContext validationContext) throws Exception {
        if (validator == null) {
            throw new UnsupportedOperationException("No suitable validator for " + getValue());
        }
        Constructor<? extends JsonValidator> c = validator.getConstructor(
                new Class[]{String.class, JsonNode.class, JsonSchema.class, ValidationContext.class, Keyword.class});
        return c.newInstance(schemaPath + "/" + getValue(), schemaNode, parentSchema, validationContext, delegate);
    }

}
